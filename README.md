# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is an educational project in order to
understand better VueJs. 
It is the alpha version and will be more versions 
using best practices, more VueJS utilities like components,slots,mixins etc.
The final version will be made using Webpack, Nuxt, Vuex, Vue Router etc.

For now the next version with just ES5 best practices and comments.

### How do I get set up? ###

For the alpha version you only have to download it.
Then just open the index.html.


### Who do I talk to? ###

If you want to contact me please email me at:
sergio_kagiema@live.com