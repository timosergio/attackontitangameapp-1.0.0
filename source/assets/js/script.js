//http://bulma.io/documentation/components/modal/

window.gameProj = {};

(function(window, document, $, undefined){
  
  "use strict";

  window.gameProj = window.gameProj || {};

  gameProj.init = function() {

    this.initVueJs = function() {
      
      
      var vm = new Vue({
        el: '#vue-app',
        data: {
          message: '',
          isActive: true,
          activeRound: true,
          modalOpened: false,
          playerLife: 100,
          enemyLife: 100,
          playerDamageDealt: 0,
          enemyDamageDealt: 0,
          playersList: [
            'Eren'
          ],
          enemiesList: [
            'Colossal Titan'
          ],
          statusList: [
          ]
        },
        methods: {
          initGamePlay: function() {

            // it should be a function like initPageTransitionAnimation whatever
            // refactor
            var layer1 = document.querySelector('.page-layer-1');
            var layer2 = document.querySelector('.page-layer-2');

            var tween2 =TweenLite.to(layer1, 1.1, { width: "100%", delay: .2 });
            var tween3 =TweenLite.to(layer2, 1, {  width: "100%" });
            var _app = this;
            setTimeout(function(){  
              // weird... how can I refactor this one?
              _app.isActive = !_app.isActive;
              tween2.reverse();
              tween3.reverse();
            },3000);

          },
          initCalculateDamage: function() {
            var d = Math.ceil(Math.random()*10) * 3; // 3 is a random number
            return d;
          },
          initPlayerAttack: function() {

            if(this.playerLife === 0 || this.enemyLife === 0) {
              return;
            }

            this.playerDamageDealt = this.initCalculateDamage();
            console.log('Player attacked!!!');
            this.statusList.unshift({
              "statText": "Eren dealt " + this.playerDamageDealt + " damage points!"
            });
            this.activeRound = !this.activeRound;
            console.log(this.playerDamageDealt);
            
          },
          initEnemyAttack: function() {

            if(this.playerLife === 0 || this.enemyLife === 0) {
              return;
            }



            this.enemyDamageDealt = this.initCalculateDamage();
            console.log('Titan attacked!!!');
            this.statusList.unshift({
              "statText": "Colossan Titan dealt " + this.enemyDamageDealt + " damage points!"
            });            
            this.activeRound = !this.activeRound;
            console.log(this.enemyDamageDealt);
            
          },
          initSpecialAttack: function() {
            var _damage = this.initCalculateDamage();
            var _special_attack = _damage * 2;
            this.playerDamageDealt = _special_attack;
            console.log('Player special attacked!!!');
            this.statusList.unshift({
              "statText": "Eren dealt " + this.playerDamageDealt + " damage points!"
            });            
            this.activeRound = !this.activeRound;
            console.log(this.playerDamageDealt);            
          },
          initHeal: function() {
            if(this.playerLife < 90  ) {
              this.playerLife = this.playerLife + this.initCalculateDamage();
              console.log('Player healed!!!');
              this.statusList.unshift({
                "statText": "Eren used Heal"
              });              
              this.activeRound = !this.activeRound;
            }
            else {
              this.playerLife = 100;
              console.log('Player healed!!!');
              this.statusList.unshift({
                "statText": "Eren used Heal"
              });              
              this.activeRound = !this.activeRound;
            }
          },
          initGiveUp: function() {
            this.activeRound = true;
            this.modalOpened = false;
            this.playerLife = 100;
            this.enemyLife = 100;
            this.playerDamageDealt = 0;
            this.enemyDamageDealt = 0;
            this.statusList = [];
            var _buttons = document.querySelectorAll('.btn-containers button');
            _buttons.forEach(function(item){
              item.removeAttribute('disabled')
            })
          },
          initEndGame: function() {
            var _buttons = document.querySelectorAll('.players-choices button');
            _buttons.forEach(function(item){ item.setAttribute('disabled', 'disabled') })
            
          },
          initGoToLoadPage: function() {
            window.location.reload();
          }
        },
        computed: {
          
          playerLifeObj: function() {
            this.playerLife = this.playerLife - this.enemyDamageDealt;
            this.enemyDamageDealt = 0;

            if(this.playerLife < this.enemyDamageDealt){
              this.playerLife = 0;
              this.initEndGame();
              this.message = 'You Lost';
              return {
                "width": this.playerLife + "%"
              }
            }

            return {
              "width" : this.playerLife + "%"
            }
          },

          enemyLifeObj: function() {
            this.enemyLife = this.enemyLife - this.playerDamageDealt;
            this.playerDamageDealt = 0;
            

            if(this.enemyLife < this.playerDamageDealt){
              this.enemyLife = 0;
              this.initEndGame();
              this.message = 'You Won';
              return {
                "width": this.enemyLife + "%"
              }
            }

            return {
              "width" : this.enemyLife + "%"
            }
          },

          showModal: function() {

            if(this.playerLife === 0 || this.enemyLife === 0 ) {
              this.modalOpened = !this.modalOpened;
              console.log(this.modalOpened)

              return this.modalOpened;
            }
          }
          
        },
        watch: {
          activeRound: function() {
            // this about when the enemy is going to 
            // attack
            var _self = this;
            var _buttons = document.querySelectorAll('.btn-containers button');

            if(this.activeRound) {

              _buttons.forEach(function(item){
                item.removeAttribute('disabled')
              });

              return;
            }
            
            setTimeout(function(){
              _self.initEnemyAttack();
            }, 1400);
            
            _buttons.forEach(function(item){ item.setAttribute('disabled', 'disabled') });

          }
        }
      });

      console.log(vm);

      var tween =TweenLite.to(document.querySelector('.site-wrapper'), 30, {  "-webkit-filter": "brightness(1)" }).delay(5);

    };

    this.initSound = function(obj) {
    
      // config the volume
      obj.volume = 0.03;
      obj.play();
      console.log('yeah it worked', obj)
      return obj;
    };

    //invokations
    this.initVueJs();

  };

  window.onload = gameProj.init();

  // when the document is ready
  // run these functions
  $(function(){
    var _sound = gameProj.initSound(document.querySelector('#sound-ongame'));
  })

})(window, document, jQuery);